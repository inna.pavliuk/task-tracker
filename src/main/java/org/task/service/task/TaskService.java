package org.task.service.task;

import org.task.entities.Task;

import java.util.List;

public interface TaskService {

    Task create(Task task);

    Task setTaskToUser(long userId, long taskId);

    Task update(Task task);

    List<Task> getTasks();

    Task getTaskById(Long id);

}
