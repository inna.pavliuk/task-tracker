package org.task.service.task;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.task.dao.TaskDao;
import org.task.dao.UserDao;
import org.task.entities.Task;
import org.task.entities.User;
import org.task.service.exceptions.BadRequestException;
import org.task.service.exceptions.TaskNotFoundException;
import org.task.service.exceptions.UserIsDeletedException;
import org.task.service.exceptions.UserNotFoundException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskDao taskDao;
    private final UserDao userDao;

    @Override
    public Task create(Task task) {
        User reporterById = userDao.findById(task.getReporter());

        if (reporterById == null)
            throw new UserNotFoundException(task.getReporter());
        if (reporterById.isDeleted())
            throw new UserIsDeletedException(task.getReporter());
        if (task.getAssignee() != null) {
            User assigneeById = userDao.findById(task.getAssignee());
            if (assigneeById == null)
                throw new UserNotFoundException(task.getAssignee());
            if (assigneeById.isDeleted())
                throw new UserIsDeletedException(task.getAssignee());
        }

        return taskDao.create(task);
    }

    @Override
    public Task setTaskToUser(long userId, long taskId) {
        Task findById = taskDao.findById(userId);
        if (findById == null)
            throw new TaskNotFoundException(taskId);
        User assigneeById = userDao.findById(userId);
        if (assigneeById == null)
            throw new UserNotFoundException(userId);
        if (assigneeById.isDeleted())
            throw new UserIsDeletedException(userId);
        findById.setAssignee(userId);
        return taskDao.update(findById);
    }

    @Override
    public Task update(Task task) {
        if (task == null || task.getId() == null || (task.getTitle() == null && task.getDescription() == null))
            throw new BadRequestException();
        Task findById = taskDao.findById(task.getId());
        if (findById == null)
            throw new TaskNotFoundException(task.getId());
        if (task.getTitle() != null)
            findById.setTitle(task.getTitle());

        if (task.getDescription() != null)
            findById.setDescription(task.getDescription());

        return taskDao.update(findById);
    }

    @Override
    public List<Task> getTasks() {
        return taskDao.getTasks();
    }

    @Override
    public Task getTaskById(Long id) {
        Task task = taskDao.findById(id);
        if (task == null)
            throw new TaskNotFoundException(id);
        return task;
    }

}
