package org.task.service.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.task.dao.TaskDao;
import org.task.dao.UserDao;
import org.task.entities.Task;
import org.task.entities.User;
import org.task.service.exceptions.BadRequestException;
import org.task.service.exceptions.UserIsDeletedException;
import org.task.service.exceptions.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final TaskDao taskDao;

    @Override()
    public User create(User user) {
        User userByEmail = userDao.findByEmail(user.getEmail());
        if (userByEmail != null)
            throw new BadRequestException();
        return userDao.create(user);
    }

    @Override
    public User update(User user) {
        if (user == null || user.getId() == null)
            throw new BadRequestException();
        User findById = userDao.findById(user.getId());
        if (findById == null)
            throw new UserNotFoundException(user.getId());
        if (user.getName() != null)
            findById.setName(user.getName());

        if (user.getEmail() != null)
            findById.setEmail(user.getEmail());

        return userDao.update(findById);
    }

    @Override
    public User delete(User user) {
        if (user.getId() == null)
            throw new BadRequestException();
        User findById = userDao.findById(user.getId());
        if (findById == null)
            throw new UserNotFoundException(user.getId());
        if (findById.isDeleted())
            throw new UserIsDeletedException(user.getId());
        return userDao.delete(user.getId());
    }

    @Override
    public User getUserById(Long id) {
        User user = userDao.findById(id);
        if (user == null)
            throw new UserNotFoundException(id);
        List<Task> allTasks = taskDao.getTasks();
        if (allTasks.isEmpty()) {
            user.setTasks(allTasks);
            return user;
        }
        List<Task> userTasks = new ArrayList<>();
        for (Task task : allTasks) {
            if (task.getAssignee().equals(id)) {
                userTasks.add(task);
            }
        }
        user.setTasks(userTasks);
        return user;
    }

    @Override
    public List<User> getUsers() {
        return userDao.getUsers();
    }
}
