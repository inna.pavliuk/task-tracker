package org.task.service.user;

import org.task.entities.User;

import java.util.List;

public interface UserService {

    User create(User user);

    User update(User user);

    User delete(User user);

    User getUserById(Long id);

    List<User> getUsers();
}
