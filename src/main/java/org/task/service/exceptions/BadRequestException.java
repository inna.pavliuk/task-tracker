package org.task.service.exceptions;

public class BadRequestException extends BaseApplicationException {

    public BadRequestException() {
        super("Bad Request: Required parameters are empty");
    }
}
