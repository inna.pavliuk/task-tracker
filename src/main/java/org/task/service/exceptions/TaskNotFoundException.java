package org.task.service.exceptions;

public class TaskNotFoundException extends BaseApplicationException {

    public TaskNotFoundException(Long id) {
        super("Task by taskId " + id + " not found");
    }
}
