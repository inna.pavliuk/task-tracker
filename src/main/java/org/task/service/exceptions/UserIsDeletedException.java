package org.task.service.exceptions;

public class UserIsDeletedException extends BaseApplicationException {

    public UserIsDeletedException(Long id) {
        super("User by id: " + id + " is deleted");
    }
}
