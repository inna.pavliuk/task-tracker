package org.task.service.exceptions;

public class UserNotFoundException extends BaseApplicationException {

    public UserNotFoundException(Long id) {
        super("User by userId " + id + " not found");
    }
}
