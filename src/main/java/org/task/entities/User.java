package org.task.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    private Long id;
    @NotBlank
    private String name;
    @NotNull
    @Email
    private String email;
    private boolean deleted;

    private List<Task> tasks;
}
