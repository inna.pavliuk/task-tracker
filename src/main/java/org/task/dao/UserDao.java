package org.task.dao;

import org.springframework.stereotype.Repository;
import org.task.entities.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserDao {

    private final Map<Long, User> users = new HashMap<>();
    private long counter = 1;

    public User create(User user) {
        user.setId(counter++);
        users.put(user.getId(), user);
        return users.get(user.getId());
    }

    public User findByEmail(String email) {
        for (User user : users.values()) {
            if (user.getEmail().equals(email)) return user;
        }
        return null;
    }

    public User findById(Long id) {
        return users.get(id);
    }

    public User delete(long id) {
        User user = users.get(id);
        user.setDeleted(true);
        users.put(id, user);
        return users.get(id);
    }

    public List<User> getUsers() {
        return new ArrayList<>(users.values());
    }

    public User update(User user) {
        users.put(user.getId(), user);
        return users.get(user.getId());
    }
}
