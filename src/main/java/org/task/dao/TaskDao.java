package org.task.dao;

import org.springframework.stereotype.Repository;
import org.task.entities.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskDao {

    private final Map<Long, Task> tasks = new HashMap<>();
    private long counter = 1;

    public Task create(Task task) {
        task.setId(counter++);
        tasks.put(task.getId(), task);
        return tasks.get(task.getId());
    }

    public Task findById(Long id) {
        return tasks.get(id);
    }

    public Task update(Task task) {
        tasks.put(task.getId(), task);
        return tasks.get(task.getId());
    }

    public List<Task> getTasks() {
        return new ArrayList<>(tasks.values());
    }
}
