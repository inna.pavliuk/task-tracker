package org.task.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.task.entities.Task;
import org.task.service.task.TaskService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("tasks")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService service;

    @PutMapping
    public ResponseEntity<Task> create(@Valid @RequestBody Task task) {
        return new ResponseEntity<>(service.create(task), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Task> update(@Valid @RequestBody Task task) {
        return new ResponseEntity<>(service.update(task), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Task>> getTasks() {
        return new ResponseEntity<>(service.getTasks(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<Task> getTaskById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getTaskById(id), HttpStatus.OK);
    }

}
